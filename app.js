const exp=require("express")
const appServer=exp()
const port=1928;

const parser= require("body-parser")

appServer.use(parser.json())

const jsonBody=parser.json()

appServer.get("/", function(req,res){
    res.send("Persija Jakarta");
});

appServer.get("/mahasiswa3si01",function(req,res){
    var mhs=["akbar","agil","akhmad","deni","fidly","ilyas","panji","rahmat","ridwan","rivaldi","rosa","syafna"];
    res.send(mhs);
});

appServer.post("/tambahmahasiswa3si01",jsonBody,function(req,res){
    res.send('Selamat Datang Ditambah mahasiswa dengan method post');
});

appServer.put('/methodput', function (req, res) {
    res.send('Ini merupakan Halaman method put');
  });

appServer.delete('/methoddelete', function (req, res) {
    res.send('Ini merupakan Halaman method delete')
  })

appServer.listen(port,function(req,res){
    console.log("PERSIJA "+port);
});